<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/posts','PostController');

Route::group(['prefix'=>'comments'], function(){
    Route::get('{comment}/like','CommentController@like');
    Route::get('{comment}/unlike','CommentController@unlike');
    Route::post('{post}','CommentController@store');
    Route::get('{post}/parent/{comment}','CommentController@create');
});

Route::group(['prefix'=>'wp-admin','namespace'=>'Admin','middleware'=>['auth','admin']], function(){
    Route::get('/','AdminController@index');
    Route::resource('/posts','PostController');
});

Route::get('/category/{category}','CategoryController@index');

Route::get('/user/posts','UserController@posts')->middleware('auth');
Route::get('/user/favorites','UserController@favorites')->middleware('auth')->name('users.favorites');
