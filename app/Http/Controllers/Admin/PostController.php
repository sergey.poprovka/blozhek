<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $where = [];
        $query = Post::with('author');
        if(request()->title){
            $where[] = ['title','LIKE','%'.request()->title.'%'];
        }
        if(request()->author){
            $query->whereHas('author', function ($q){
                $q->where('name','LIKE','%'.request()->author.'%');
            });
        }
        if(request()->date){
            $where[] = ['created_at','>', date('Y-m-d H:i:s', strtotime(request()->date))];
        }
        $posts = $query->where($where)->orderBy('id','DESC')->paginate(env('PER_PAGE'));

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|string',
            'categories'=>'required|array',
            'textcontent'=>'nullable|string'
        ]);

        $post = Post::create([
            'title' => $request->title,
            'content' => $request->textcontent,
            'author_id' => auth()->user()->id
        ]);

        $post->categories()->sync($request->categories, true);

        if($post){
            Session::flash('success',__('Post created'));
            return redirect('/wp-admin/posts');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $cats = $post->categories->pluck('id')->toArray();

        return view('admin.posts.edit', compact('post', 'cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title'=>'required|string',
            'categories'=>'required|array',
            'textcontent'=>'nullable|string'
        ]);

        $post->title = $request->title;
        $post->content = $request->textcontent;

        $post->categories()->sync($request->categories, true);

        if($post->save()){
            Session::flash('success',__('Post updated'));
            return redirect('/wp-admin/posts');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {

        $post->delete();

        Session::flash('danger',__('Post deleted'));

        return redirect()->back();
    }
}
