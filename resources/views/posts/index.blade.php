@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h1>{{ __('Posts') }}</h1>
                    <a href="/posts/create" class="btn btn-outline-success">{{ __('Create post') }}</a>
                </div>
                <div class="filters mb-3">
                    <form method="get" action="/posts">
                        <div class="card card-body">
                            <div class="row">
                                <div class="col-5">
                                    <input class="form-control" type="text" name="title" value="{{ request()->title }}" placeholder="{{ __('Post title') }}..." />
                                </div>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="author" value="{{ request()->author }}" placeholder="{{ __('Post author') }}..." />
                                </div>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="date" value="{{ request()->date }}" placeholder="{{ __('Posted after') }}..." />
                                </div>
                                <div class="col-1">
                                    <button type="submit" class="btn btn-success">{{__('Search')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @if(count($posts))
                    @foreach($posts as $post)
                            <div class="card card-body mb-3 shadow-lg">
                                <a href="/posts/{{ $post->id }}"><h3>{{ $post->title }}</h3></a>
                                <p>{{ $post->preview }}</p>
                                <p><strong>{{ __('Author') }}:</strong> {{ $post->author->name }}</p>
                                <p><strong>{{ __('Created') }}:</strong> {{ $post->created_at->diffForHumans() }}</p>
                                <ul class="list-unstyled list-inline">
                                    @foreach($post->categories as $category)
                                        <li class="list-inline-item">
                                            <a href="/category/{{ $category->slug }}">
                                                <span class="badge badge-primary">{{ $category->name }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                    @endforeach
                    <div class="pagination">
                        {{ $posts->appends(request()->input())->links() }}
                    </div>
                @else
                    <div class="alert alert-danger mt-5 mb-5">
                        {{__('Nothing here for now. Sorry...')}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
