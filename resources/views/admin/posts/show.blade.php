@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>{{ $post->title }}</h1>
                <ul class="list-unstyled list-inline">
                    @foreach($post->categories as $category)
                        <li class="list-inline-item">
                            <a href="/category/{{ $category->slug }}">
                                <span class="badge badge-primary">{{ $category->name }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <p>{{ $post->content }}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div>{{ __('Author') }}: {{ $post->author->name }}</div>
                    <div>{{ __('Created') }}: {{ $post->created_at->diffForHumans() }}({{ $post->created_at->format('H:i:s d M Y') }})</div>
                </div>
            </div>
            <hr />

        </div>
    </div>
@endsection
