@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h1>{{ __('Posts') }}</h1>
                    <a href="/posts/create" class="btn btn-outline-success">{{ __('Create post') }}</a>
                </div>
                <div class="filters mb-3">
                    <form method="get" action="/posts">
                        <div class="card card-body">
                            <div class="row">
                                <div class="col-5">
                                    <input class="form-control" type="text" name="title" value="{{ request()->title }}" placeholder="{{ __('Post title') }}..." />
                                </div>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="author" value="{{ request()->author }}" placeholder="{{ __('Post author') }}..." />
                                </div>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="date" value="{{ request()->date }}" placeholder="{{ __('Posted after') }}..." />
                                </div>
                                <div class="col-1">
                                    <button type="submit" class="btn btn-success">{{__('Search')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>{{__('Title')}}</th>
                        <th>{{__('Author')}}</th>
                        <th>{{__('Categories')}}</th>
                        <th>{{__('Actions')}}</th>
                    </tr>
                    @if(count($posts))
                        @foreach($posts as $post)
                            <tr>
                            <td><a href="/wp-admin/posts/{{ $post->id }}">{{ $post->title }}</a></td>
                            <td>{{ $post->author->name }}</td>
                            <td>
                                <ul class="list-unstyled list-inline">
                                    @foreach($post->categories as $category)
                                        <li class="list-inline-item">
                                            <a href="/category/{{ $category->slug }}">
                                                <span class="badge badge-primary">{{ $category->name }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>
                                <a href="/wp-admin/posts/{{ $post->id }}" class="btn btn-primary"><i class="fas fa-eye"></i> </a>
                                <a href="/wp-admin/posts/{{ $post->id }}/edit" class="btn btn-warning"><i class="fas fa-pen"></i> </a>
                                <form method="post" action="/wp-admin/posts/{{ $post->id }}" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger"><i class="fas fa-times"></i></button>
                                </form>
                            </td>
                            </tr>
                        @endforeach
                </table>
                <div class="pagination">
                    {{ $posts->appends(request()->input())->links() }}
                </div>
                    @else
                        <div class="alert alert-danger mt-5 mb-5">
                            {{__('Nothing here for now. Sorry...')}}
                        </div>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
